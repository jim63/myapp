import 'package:fijkplayer/fijkplayer.dart';
import 'package:flutter/material.dart';
import 'package:my_app/my_drawer.dart';

class StreamPage extends StatefulWidget {
  final String url;
  StreamPage({@required this.url});
  @override
  _StreamPageState createState() => _StreamPageState();
}

class _StreamPageState extends State<StreamPage> {
  final FijkPlayer player = FijkPlayer();
  _StreamPageState();
  @override
  void initState() {
    super.initState();
    player.setDataSource(widget.url, autoPlay: true);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Stream player")),
        drawer: MyDrawer(),
        body: Container(
          alignment: Alignment.center,
          child: FijkView(
            player: player,
          ),
        ));
  }

  @override
  void dispose() {
    super.dispose();
    player.release();
  }
}
