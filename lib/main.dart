import 'package:camera/camera.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:my_app/models/restaurant.dart';
import 'package:my_app/pages/food_page/food_page.dart';
import 'package:my_app/pages/login_page/login_page.dart';
import 'package:my_app/pages/tic_tok_page/tic_tok_page.dart';
import 'package:my_app/pages/up_stream_page/up_stream_page.dart';
import 'package:my_app/pages/video_page/video_page.dart';
import 'package:my_app/providers/counterNotifier.dart';
import 'package:my_app/providers/pageNotifier.dart';
import 'package:my_app/services/firestore_service.dart';
import 'package:my_app/stream_page/stream_page.dart';
import 'package:provider/provider.dart';

import 'my_drawer.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final cameras = await availableCameras();
  final camera = cameras.first;
//  debugPaintSizeEnabled = true;
  runApp(MyApp(camera: camera));
}

class MyApp extends StatelessWidget {
  final CameraDescription camera;

  MyApp({
    Key key,
    @required this.camera,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final FireStoreService _db = FireStoreService();
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: MyCountChangeNotifier()),
        ChangeNotifierProvider.value(value: MyPageChangeNotifier()),
        StreamProvider<FirebaseUser>.value(
          value: FirebaseAuth.instance.onAuthStateChanged,
        ),
        StreamProvider<List<Restaurant>>(
          create: (BuildContext context) => _db.streamRestaurants(),
        ),
      ],
      child: MaterialApp(
          debugShowCheckedModeBanner: false,
          home: Scaffold(
            body: HomePage(camera: camera),
          )),
    );
  }
}

class HomePage extends StatelessWidget {
  final CameraDescription camera;
  HomePage({
    Key key,
    @required this.camera,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final currentPage = Provider.of<MyPageChangeNotifier>(context);
    final List pages = [
      LoginPage(),
      FoodPage(),
      TicTokPage(),
      CounterPage(),
      VideoPage(),
      StreamPage(url: 'rtmp://dev.dentall.site/live/livestream'),
      UpStreamPage(),
    ];
    return pages[currentPage.page];
  }
}

//
class CounterPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //透過 Provider.of 來獲取資料
    final counter = Provider.of<MyCountChangeNotifier>(context);

    return Scaffold(
      drawer: MyDrawer(),
      appBar: AppBar(
        title: Text('Food page'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              '目前計數值: ${counter.count}',
            ),
            RaisedButton(
              //點擊按鈕後，導轉跳到B頁
              onPressed: () => Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => BPage()),
              ),
              child: Text('跳到B頁'),
            ),
          ],
        ),
      ),
    );
  }
}

class BPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('B page'),
      ),
      body: Center(
        // 透過 Consumer 來接料收更改對應資
        child: Consumer<MyCountChangeNotifier>(builder: (
          context,
          counter,
          _,
        ) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                '目前計數值:',
              ),
              Text(
                '${counter.count}',
              ),
            ],
          );
        }),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          // 使用 Provider.of，並且將 listen 設定為 false(若沒設定，預設為 true)，
          // 則不會再次調用 Widget 重新構建（ build ）畫面 ，更省效能。
          Provider.of<MyCountChangeNotifier>(context, listen: false)
              .increment();
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
