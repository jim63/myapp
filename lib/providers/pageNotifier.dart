import 'package:flutter/foundation.dart';

class MyPageChangeNotifier with ChangeNotifier {
  int _page = 0;

  int get page => _page;

  void goto(page) {
    _page = page;
    notifyListeners();
  }
}
