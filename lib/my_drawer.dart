import 'package:flutter/material.dart';
import 'package:my_app/providers/pageNotifier.dart';
import 'package:provider/provider.dart';

import 'my_drawer_header.dart';

class MyDrawer extends StatefulWidget {
  @override
  _MyDrawerState createState() => _MyDrawerState();
}

class _MyDrawerState extends State<MyDrawer> {
  @override
  Widget build(BuildContext context) {
    final currentPage = Provider.of<MyPageChangeNotifier>(context);

    return Drawer(
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.all(0),
        children: <Widget>[
          MyDrawerHeader(),
          ListTile(
            selected: currentPage.page == 0,
            leading: Icon(Icons.card_membership),
            title: Text(
              'login',
              style: TextStyle(
                fontSize: 20,
              ),
            ),
            onTap: () {
              Provider.of<MyPageChangeNotifier>(context, listen: false).goto(0);
              Navigator.of(context).pop();
            },
          ),
          ListTile(
            selected: currentPage.page == 1,
            leading: Icon(Icons.fastfood),
            title: Text(
              'Food',
              style: TextStyle(
                fontSize: 20,
              ),
            ),
            onTap: () {
              Provider.of<MyPageChangeNotifier>(context, listen: false).goto(1);
              Navigator.of(context).pop();
            },
          ),
          ListTile(
            selected: currentPage.page == 2,
            leading: GestureDetector(
              child: Text('123'),
            ),
            title: Text(
              'game',
              style: TextStyle(
                fontSize: 20,
              ),
            ),
            onTap: () {
              Provider.of<MyPageChangeNotifier>(context, listen: false).goto(2);
              Navigator.of(context).pop();
            },
          ),
          ListTile(
            selected: currentPage.page == 3,
            leading: Icon(Icons.plus_one),
            title: Text(
              'counter',
              style: TextStyle(
                fontSize: 20,
              ),
            ),
            onTap: () {
              Provider.of<MyPageChangeNotifier>(context, listen: false).goto(3);
              Navigator.of(context).pop();
            },
          ),
          ListTile(
            selected: currentPage.page == 4,
            leading: Icon(Icons.video_call),
            title: Text(
              'video',
              style: TextStyle(
                fontSize: 20,
              ),
            ),
            onTap: () {
              Provider.of<MyPageChangeNotifier>(context, listen: false).goto(4);
              Navigator.of(context).pop();
            },
          ),
          ListTile(
            selected: currentPage.page == 5,
            leading: Icon(Icons.view_stream),
            title: Text(
              'stream',
              style: TextStyle(
                fontSize: 20,
              ),
            ),
            onTap: () {
              Provider.of<MyPageChangeNotifier>(context, listen: false).goto(5);
              Navigator.of(context).pop();
            },
          ),
          ListTile(
            selected: currentPage.page == 6,
            leading: Icon(Icons.view_stream),
            title: Text(
              'upstream',
              style: TextStyle(
                fontSize: 20,
              ),
            ),
            onTap: () {
              Provider.of<MyPageChangeNotifier>(context, listen: false).goto(6);
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }
}
