import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:my_app/models/restaurant.dart';

class FireStoreService {
  Firestore _db = Firestore.instance;

  Stream<List<Restaurant>> streamRestaurants() {
    return _db
        .collection('/restaurant')
        .orderBy('count', descending: true)
        .snapshots()
        .map((snapshot) => snapshot.documents
            .map((document) => Restaurant.fromFirestore(document))
            .toList());
  }

  Future<void> deleteRestaurant(String documentID) {
    Firestore.instance.collection('/restaurant').document(documentID).delete();
    return null;
  }

  Future<void> createRestaurant(Restaurant restaurant) {
    Firestore.instance.collection('/restaurant').document().setData({
      'name': restaurant.name,
      'content': restaurant.content,
    });
    return null;
  }

  Future<void> updateRestaurant(Restaurant restaurant, String documentID) {
    Firestore.instance
        .collection('/restaurant')
        .document(documentID)
        .updateData({
      'name': restaurant.name,
      'content': restaurant.content,
      'count': restaurant.count,
    });
    return null;
  }
}
