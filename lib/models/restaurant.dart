import 'package:cloud_firestore/cloud_firestore.dart';

class Restaurant {
  final String name;
  final String content;
  final String documentID;
  final int count;

  Restaurant({this.name, this.content, this.documentID, this.count});

  factory Restaurant.fromFirestore(DocumentSnapshot doc) {
    Map data = doc.data;
    return Restaurant(
      documentID: doc.documentID,
      name: data['name'] ?? 'default name',
      content: data['content'] ?? 'default content',
      count: data['count'] ?? 0,
    );
  }
}
