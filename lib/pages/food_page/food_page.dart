import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:my_app/models/restaurant.dart';
import 'package:my_app/pages/food_page/food_game_page.dart';
import 'package:my_app/pages/food_page/food_list_page.dart';
import 'package:my_app/services/firestore_service.dart';

import '../../my_drawer.dart';

class FoodPage extends StatefulWidget {
  @override
  _FoodPageState createState() => _FoodPageState();
}

class _FoodPageState extends State<FoodPage> {
  int _currentPage = 0;
  @override
  Widget build(BuildContext context) {
    List<Widget> views = [FoodListPage(), FoodGamePage()];
    FireStoreService _db = FireStoreService();
    print('_currentPage $_currentPage');
    return Scaffold(
      drawer: MyDrawer(),
      appBar: AppBar(
        title: Text('Food page'),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.plus_one),
        onPressed: () {
          showAddDialog(context);
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
          shape: CircularNotchedRectangle(),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              IconButton(
                icon: Icon(Icons.list),
                onPressed: () {
                  setState(() {
                    _currentPage = 0;
                  });
                },
              ),
              IconButton(
                icon: Icon(Icons.videogame_asset),
                onPressed: () {
                  setState(() {
                    print(1);
                    _currentPage = 1;
                  });
                },
              )
            ],
          )),
      body: views[_currentPage],
    );
  }

  void showAddDialog(BuildContext context) {
    final focus = FocusNode();
    final TextEditingController nameController = TextEditingController();
    final TextEditingController descController = TextEditingController();
    FireStoreService _db = FireStoreService();

    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('新增餐廳'),
            titleTextStyle: TextStyle(
              fontSize: 20,
              color: Colors.blueAccent,
              fontWeight: FontWeight.bold,
            ),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                TextField(
                  autofocus: true,
                  maxLength: 10,
                  decoration: InputDecoration(
                    labelText: 'name',
                  ),
                  textInputAction: TextInputAction.next,
                  onEditingComplete: () {
                    FocusScope.of(context).requestFocus(focus);
                  },
                  controller: nameController,
                ),
                TextField(
                  focusNode: focus,
                  maxLength: 10,
                  decoration: InputDecoration(
                    labelText: 'description',
                  ),
                  controller: descController,
                ),
              ],
            ),
            actions: [
              FlatButton(
                child: Text('cancel'.toUpperCase()),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              FlatButton(
                child: Text('ok'.toUpperCase()),
                onPressed: () {
                  if (nameController.text.isNotEmpty &&
                      descController.text.isNotEmpty) {
                    Restaurant restaurant = Restaurant(
                      name: nameController.text,
                      content: descController.text,
                    );

                    _db.createRestaurant(restaurant);

                    Navigator.of(context).pop();
                  }
                },
              )
            ],
          );
        });
  }
}
