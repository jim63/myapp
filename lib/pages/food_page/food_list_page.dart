import 'package:flutter/material.dart';
import 'package:my_app/models/restaurant.dart';
import 'package:my_app/services/firestore_service.dart';
import 'package:provider/provider.dart';

class FoodListPage extends StatefulWidget {
  @override
  _FoodListPageState createState() => _FoodListPageState();
}

class _FoodListPageState extends State<FoodListPage> {
  @override
  Widget build(BuildContext context) {
    final FireStoreService _db = FireStoreService();
    List<Restaurant> restaurants = Provider.of<List<Restaurant>>(context);
    print('restaurants $restaurants');
    if (restaurants == null) {
      return Center(
        child: Text('現在沒有資料'),
      );
    }

    return ListView.builder(
      itemCount: restaurants.length,
      itemBuilder: (context, index) {
        Restaurant restaurant = restaurants[index];
        return GestureDetector(
          onTap: () {
            Restaurant newRestaurant = Restaurant(
              name: restaurant.name,
              content: restaurant.content,
              count: restaurant.count + 1,
            );
            _db.updateRestaurant(newRestaurant, restaurant.documentID);
          },
          child: Dismissible(
            key: Key(restaurant.documentID),
            child: (Card(
                child: ListTile(
              title: Text('${restaurant.name}:   ${restaurant.count}'),
              subtitle: Text(restaurant.content),
            ))),
            background: Background(),
            secondaryBackground: SecondBackground(),
            confirmDismiss: (direction) async {
              if (direction == DismissDirection.endToStart) {
                await _db.deleteRestaurant(restaurant.documentID);
                return true;
              } else if (direction == DismissDirection.startToEnd) {
                return false;
              }
              return false;
            },
          ),
        );
      },
    );
//      },
//    );
  }
}

class Background extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.green,
      child: Align(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              width: 20,
            ),
            Icon(
              Icons.edit,
              color: Colors.white,
            ),
            Text(
              " Edit",
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w700,
              ),
              textAlign: TextAlign.left,
            ),
          ],
        ),
        alignment: Alignment.centerLeft,
      ),
    );
  }
}

class SecondBackground extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.red,
      child: Align(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Icon(
              Icons.delete,
              color: Colors.white,
            ),
            Text(
              " Delete",
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w700,
              ),
              textAlign: TextAlign.right,
            ),
            SizedBox(
              width: 20,
            ),
          ],
        ),
        alignment: Alignment.centerRight,
      ),
    );
  }
}
