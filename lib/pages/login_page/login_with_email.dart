import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class LoginWithEmail extends StatefulWidget {
  @override
  _LoginWithEmailState createState() => _LoginWithEmailState();
}

class _LoginWithEmailState extends State<LoginWithEmail> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  final focus = FocusNode();
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Column(mainAxisAlignment: MainAxisAlignment.center, children: [
      FlutterLogo(
        size: 150,
      ),
      Form(
        key: _formKey,
        child: Container(
          width: 250,
          child: Column(
            children: [
              TextFormField(
                decoration: InputDecoration(
                  hintText: 'email',
                ),
                controller: emailController,
                validator: (value) {
                  if (value.isEmpty) {
                    return 'email must not be null';
                  }
                  return null;
                },
                textInputAction: TextInputAction.next,
                onEditingComplete: () {
                  FocusScope.of(context).requestFocus(focus);
                },
              ),
              TextFormField(
                decoration: InputDecoration(
                  hintText: 'password',
                ),
                focusNode: focus,
                controller: passwordController,
                obscureText: true,
                validator: (value) {
                  if (value.isEmpty) {
                    return 'password must not be null';
                  }
                  return null;
                },
              ),
              RaisedButton(
                child: Text('login'),
                onPressed: () async {
                  if (_formKey.currentState.validate()) {
                    try {
                      var user = await FirebaseAuth.instance
                          .signInWithEmailAndPassword(
                              email: emailController.text,
                              password: passwordController.text);
                      print(user);
                      final snackbar = SnackBar(
                        backgroundColor: Colors.green,
                        content: Text('登入成功'),
                        action: SnackBarAction(
                          label: '了',
                          onPressed: () {},
                        ),
                      );
                      FocusScope.of(context).requestFocus(FocusNode());

                      Scaffold.of(context).showSnackBar(snackbar);
                    } catch (e) {
                      print(e);
                      final snackbar = SnackBar(
                        backgroundColor: Colors.red,
                        content: Text('登入失敗'),
                        action: SnackBarAction(
                          label: '了QQ',
                          onPressed: () {},
                        ),
                      );
                      Scaffold.of(context).showSnackBar(snackbar);
                    }
                  }
                },
              ),
            ],
          ),
        ),
      )
    ]);
  }
}
