import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../my_drawer.dart';
import 'login_with_email.dart';
import 'loginin_with_google.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    var user = Provider.of<FirebaseUser>(context);
    bool loggedIn = user != null;
//    if (user != null) {
//      print(user.displayName);
//      print(user.email);
//      print(user.isEmailVerified);
//      print(user.photoUrl);
//    }

    if (loggedIn) {
      return Scaffold(
        drawer: MyDrawer(),
        appBar: AppBar(
          title: Text('Login page'),
        ),
        body: Center(
            child:
                Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Text('Logged In!!!'),
          RaisedButton(
            child: Text('logout'),
            onPressed: () {
              FirebaseAuth.instance.signOut();
              final snackbar = SnackBar(
                backgroundColor: Colors.green,
                content: Text('登出成功'),
                action: SnackBarAction(
                  label: '了',
                  onPressed: () {},
                ),
              );
              Scaffold.of(context).showSnackBar(snackbar);
            },
          )
        ])),
      );
    }

    return DefaultTabController(
      length: 2,
      child: Scaffold(
        drawer: MyDrawer(),
        appBar: AppBar(
          title: Text('Login page'),
          bottom: TabBar(
            tabs: [
              Tab(
                icon: Icon(Icons.email),
                text: "login with email",
              ),
              Tab(
                icon: Icon(Icons.monochrome_photos),
                text: "login with google",
              ),
            ],
          ),
        ),
        body: TabBarView(
          children: [
            LoginWithEmail(),
            LoginWithGoogle(),
          ],
        ),
      ),
    );
  }
}
