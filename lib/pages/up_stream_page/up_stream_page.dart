import 'package:flutter/material.dart';
import 'package:flutter_rtmp_publisher/flutter_rtmp_publisher.dart';
import 'package:my_app/my_drawer.dart';

class UpStreamPage extends StatefulWidget {
  const UpStreamPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _UpStreamPageState createState() => _UpStreamPageState();
}

class _UpStreamPageState extends State<UpStreamPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Flutter RTMP upstream'),
      ),
      drawer: MyDrawer(),
      body: Container(
        child: RaisedButton(
          child: Text("Start Stream"),
          onPressed: () {
            RTMPPublisher.streamVideo(
                "rtmp://dev.dentall.site/live/livestream");
          },
        ),
      ),
    );
  }
}
